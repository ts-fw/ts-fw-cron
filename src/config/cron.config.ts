import {Config} from 'ts-fw';
import {Container, inject} from 'inversify';
import {CronJob} from 'cron';
import {ICronScheduleModel} from '../decorators';
import * as winston from 'winston';

@Config(2)
export class CRONConfig {
    @inject('di')
    private container: Container;

    @inject('logger')
    private logger: winston.Winston;

    configuring() {
        const cronService: any[] = [];
        try {
            cronService.push(...this.container.getAll('CRON'));
        } catch (ignore) {
            this.logger.error('Cron services not found. Use @CRON() decorator for create mark of class as cron service');
            return;
        }

        for (const service of cronService) {
            const cronScheduleModels: ICronScheduleModel[] = [];
            try {
                cronScheduleModels.push(...Reflect.getMetadata('methods', service));
            } catch (ignore) {
                continue;
            }

            if (!cronScheduleModels) {
                continue;
            }

            for (const cronScheduleModel of cronScheduleModels) {
                if (typeof service[cronScheduleModel.method] !== 'function') {
                    continue;
                }

                new CronJob(cronScheduleModel.schedule, () => {
                    service[cronScheduleModel.method]();
                }).start();
            }
        }
    }
}
