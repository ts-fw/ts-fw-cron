export interface ICronScheduleModel {
    schedule: string;
    method: string;
}

export function Schedule(cronSchedule: string): MethodDecorator {
    return (constructor: any, propertyKey: string) => {
        let methods: ICronScheduleModel[];

        if (Reflect.hasMetadata('methods', constructor)) {
            methods = Reflect.getMetadata('methods', constructor);
        } else {
            methods = [];
            Reflect.defineMetadata('methods', methods, constructor);
        }

        methods.push({
            schedule: cronSchedule,
            method: propertyKey
        });
    }
}
