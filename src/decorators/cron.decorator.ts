import {Injectable} from 'ts-fw';

export function CRON(): ClassDecorator {
    return (constructor: Function) => {
        Reflect.decorate([Injectable('CRON')], constructor);
    }
}
